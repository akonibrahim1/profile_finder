## Profile Finder

```
git clone https://gitlab.com/akonibrahim1/profile_finder.git
cd profile_finder
docker-compose up
docker exec -it profile_finder_backend_1 npm --prefix ./frontend install
docker exec -it profile_finder_backend_1 npm run --prefix ./frontend build
```

Visit it using http://localhost:3001 and
Database http://localhost:5434

Note :- If you decided to change backend port. Make sure to update frontend/.env

```
REACT_APP_API_URL=http://localhost:8080/api
```
