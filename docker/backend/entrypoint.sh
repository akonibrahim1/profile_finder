#!/bin/sh

# until cd /app/backend/server
# do
#     echo "Waiting for server volume..."
# done

# until ./manage.py migrate
# do
#     echo "Waiting for db to be ready..."
#     sleep 2
# done

# ./manage.py collectstatic --noinput
# # watchgod celery.__main__.main --args -A server.celery_app worker -l INFO
# # celery multi start worker1 worker2 -l info -A server.celery_app
# # gunicorn server.wsgi --bind 0.0.0.0:8000 --workers 4 --threads 4 --reload
# ./manage.py runserver 0.0.0.0:8000

# # celery multi start worker1 worker2 -A server
# # celery -A server beat



#!/usr/bin/env bash
until cd /app/backend
do
    echo "Waiting for server volume..."
done

flask db migrate
flask db upgrade
gunicorn -b 0.0.0.0:8000 run:app --reload