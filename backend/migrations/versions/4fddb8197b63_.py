"""empty message

Revision ID: 4fddb8197b63
Revises: 
Create Date: 2022-10-22 06:04:22.582931

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '4fddb8197b63'
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table('searches',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('keywords', sa.String(length=200), nullable=False),
    sa.Column('created_at', sa.DateTime(), nullable=True),
    sa.Column('updated_at', sa.DateTime(), nullable=True),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_table('persons',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('name', sa.String(length=100), nullable=False),
    sa.Column('first_name', sa.String(length=100), nullable=True),
    sa.Column('last_name', sa.String(length=100), nullable=True),
    sa.Column('gender', sa.String(length=30), nullable=True),
    sa.Column('email', sa.String(length=100), nullable=False),
    sa.Column('image', sa.String(length=150), nullable=False),
    sa.Column('source', sa.String(length=100), nullable=False),
    sa.Column('phone', sa.String(length=100), nullable=False),
    sa.Column('country', sa.String(length=80), nullable=False),
    sa.Column('country_code', sa.String(length=10), nullable=False),
    sa.Column('info', sa.Text(), nullable=True),
    sa.Column('confirmed_match', sa.Boolean(), nullable=True),
    sa.Column('created_at', sa.DateTime(), nullable=True),
    sa.Column('updated_at', sa.DateTime(), nullable=True),
    sa.Column('search_id', sa.Integer(), nullable=True),
    sa.ForeignKeyConstraint(['search_id'], ['searches.id'], ),
    sa.PrimaryKeyConstraint('id')
    )
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_table('persons')
    op.drop_table('searches')
    # ### end Alembic commands ###
