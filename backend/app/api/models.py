import datetime

from app import db


class Search(db.Model):
    __tablename__ = "searches"
    id = db.Column(db.Integer(), primary_key=True)
    keywords = db.Column(db.String(200), nullable=False)
    created_at = db.Column(db.DateTime, default=datetime.datetime.utcnow)
    updated_at = db.Column(
        db.DateTime, default=datetime.datetime.utcnow, onupdate=datetime.datetime.utcnow
    )

    def to_dict(self):
        return {
            "id": self.id,
            "keywords": self.keywords,
            "created_at": self.created_at,
            "updated_at": self.updated_at,
            "person_count": len(self.persons),
        }

    def __repr__(self):
        return "<Search %r>" % self.id


class Person(db.Model):
    __tablename__ = "persons"
    id = db.Column(db.Integer(), primary_key=True)
    name = db.Column(db.String(100), nullable=False)
    first_name = db.Column(db.String(100), nullable=True)
    last_name = db.Column(db.String(100), nullable=True)
    gender = db.Column(db.String(30), nullable=True)
    email = db.Column(db.String(100), nullable=False)
    image = db.Column(db.String(150), nullable=False)
    source = db.Column(db.String(100), nullable=False)
    phone = db.Column(db.String(100), nullable=False)
    country = db.Column(db.String(80), nullable=False)
    country_code = db.Column(db.String(10), nullable=False)
    info = db.Column(db.Text(), nullable=True)
    confirmed_match = db.Column(db.Boolean(), default=False)
    created_at = db.Column(db.DateTime, default=datetime.datetime.utcnow)
    updated_at = db.Column(
        db.DateTime, default=datetime.datetime.utcnow, onupdate=datetime.datetime.utcnow
    )
    search_id = db.Column(db.Integer, db.ForeignKey("searches.id"))
    search = db.relationship("Search", backref="persons", foreign_keys=[search_id])

    def to_dict(self):
        return {
            "id": self.id,
            "name": self.name,
            "email": self.email,
            "image": self.image,
            "source": self.source,
            "gender": self.gender,
            "phone": self.phone,
            "confirmed_match": self.confirmed_match,
            "first_name": self.first_name,
            "last_name": self.last_name,
            "email": self.email,
            "country": self.country,
            "country_code": self.country_code,
            "info": self.info,
            "created_at": self.created_at,
            "updated_at": self.updated_at,
        }

    def __repr__(self):
        return "<Person %r>" % self.name


class SavedProfile(db.Model):
    __tablename__ = "saved_profiles"
    id = db.Column(db.Integer(), primary_key=True)
    person_id = db.Column(db.Integer, db.ForeignKey("persons.id"))
    person = db.relationship("Person", foreign_keys=[person_id])

    def to_dict(self):
        return {"id": self.id, "person": self.person.to_dict()}

    def __repr__(self):
        return "<SavedProfile %r>" % self.person.name
