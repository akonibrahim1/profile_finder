import json, os, random, re
from flask import jsonify, request

from app import db
from app.api import api
from app.settings import basedir
from .models import *


@api.route("search", methods=["POST"])
def search():
    path = os.path.join(basedir, "api", "mock_data.json")
    with open(path, "r") as file:
        mock_data = json.load(file)

    data = request.json
    keywords = data.get("keywords")
    keywords_joined = "|".join(keywords)

    search = Search.query.filter(Search.keywords == keywords_joined).first()

    if not search:
        sources = ["facebook", "instagram", "twitter", "linkedIn"]
        search = Search(keywords=keywords_joined)

        p = None
        for data in mock_data:
            for i, j in data.items():
                if (i not in ["gender", "country", "info"]) and any(
                    re.search(keyword, str(j)) for keyword in keywords
                ):
                    source = random.choices(sources)[0]
                    p = Person(
                        name=data["name"],
                        first_name=data["first_name"],
                        last_name=data["last_name"],
                        gender=data["gender"],
                        email=data["email"],
                        image=data["image"],
                        phone=data["phone"],
                        country=data["country"],
                        country_code=data["country_code"],
                        info=data["info"],
                        search=search,
                        source=source,
                    )
                    db.session.add(p)
                    break
                # Atleast one result need to be found to become history
                if p:
                    db.session.commit()

    return jsonify(
        {"search_id": search.id, "persons": [p.to_dict() for p in search.persons]}
    )


@api.route("/searches")
def get_all_searches():
    searches = Search.query.order_by(Search.id.desc()).limit(7).all()
    return jsonify([s.to_dict() for s in searches])


@api.route("/searches/<int:id>")
def get_search_persons(id):
    search = Search.query.get(id)
    return jsonify([s.to_dict() for s in search.persons])


@api.route("save_profile/<int:person_id>")
def save_profile(person_id):
    # In real time scenario this should have the current logged in user id
    saved_profile = SavedProfile.query.filter(person_id == person_id).first()
    if not saved_profile:
        saved_profile = SavedProfile(person_id=person_id)
        db.session.add(saved_profile)
        db.session.commit()

    return jsonify(saved_profile.to_dict())
