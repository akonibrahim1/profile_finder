import React, { useState, useEffect } from "react";
import { Col, Row } from "antd";
import { Button, Form, Select, Tag } from "antd";

import {
  FacebookOutlined,
  LinkedinOutlined,
  TwitterOutlined,
  InstagramOutlined,
} from "@ant-design/icons";

import { GetPersons, GetAllSearches } from "../services/ProfileServices";
import { SearchHistory } from "../components/SearchHistory";
import { PersonList } from "../components/PersonList";
import { DetailedView } from "../components/DetailedView";
import { SocialFiltersView } from "../components/SocialFiltersView";

function Home() {
  const [form] = Form.useForm();
  const [loading, setLoading] = useState(false);
  const [person, setPerson] = useState({});
  const [selected_history, setSelectedHistory] = useState(0);
  const [histories, setHistory] = useState({});
  const [persons, setPersons] = useState([]);
  const [possible, setPossible] = useState([]);
  const [confirmed, setConfirmed] = useState([]);
  const [socialFilters, setSocialFilters] = useState({});

  const children = [];
  var keywordSelect = null;
  const layout = {
    labelCol: {
      span: 5,
    },
    wrapperCol: {
      span: 19,
    },
  };
  const tailLayout = {
    wrapperCol: {
      offset: 5,
      span: 19,
    },
  };

  async function onFinish(values) {
    setLoading(true);
    await GetPersons(values).then((res) => {
      PopulateTables(res.persons);
      setSelectedHistory(res.search_id);
      LoadHistory();
    });
    setLoading(false);
  }

  async function PopulateTables(data) {
    setSocialFilters({});
    setConfirmed([]);
    await setPersons(data);
    await setPossible(data);
    var uniqueSocials = {};
    data.map((d) => {
      if (uniqueSocials[d.source] == undefined) {
        uniqueSocials[d.source] = 1;
      } else {
        uniqueSocials[d.source] += 1;
      }
    });
    setSocialFilters(uniqueSocials);
  }

  const onReset = () => {
    form.resetFields();
    setPersons([]);
    setPerson([]);
    setPossible([]);
    setConfirmed([]);
    setSocialFilters({});
  };

  const onChange = (checked) => {
    console.log(`switch to ${checked}`);
  };

  const RenderSource = (source) => {
    if (source == "facebook") {
      return (
        <Tag icon={<FacebookOutlined />} color="#3b5999">
          Facebook
        </Tag>
      );
    } else if (source == "twitter") {
      return (
        <Tag icon={<TwitterOutlined />} color="#55acee">
          Twitter
        </Tag>
      );
    } else if (source == "linkedIn") {
      return (
        <Tag icon={<LinkedinOutlined />} color="#0077b5">
          LinkedIn
        </Tag>
      );
    } else {
      return (
        <Tag icon={<InstagramOutlined />} color="rgb(248 86 162)">
          Instagram
        </Tag>
      );
    }
  };

  const LoadHistory = () => {
    GetAllSearches().then((res) => {
      setHistory(res);
      // console.log(res);
    });
  };

  const ToggleConfirmedAndPossible = (person, type) => {
    if (type == "possible") {
      let possibles = possible.filter((p_person) => p_person.id !== person.id);
      setPossible(possibles);
      if (confirmed.length) {
        confirmed.push(person);
        setConfirmed(confirmed);
      } else {
        setConfirmed([person]);
      }
    } else {
      let confirms = confirmed.filter((c_person) => c_person.id !== person.id);
      setConfirmed(confirms);
      possible.splice(0, 0, person);
      setPossible(possible);
    }
  };

  useEffect(() => {
    // pingServer().then((res) => console.log(res));
  }, []);

  return (
    <>
      <Row style={{ marginTop: "50px" }}>
        <Col span={5}>
          <SearchHistory
            PopulateTables={PopulateTables}
            LoadHistory={LoadHistory}
            histories={histories}
            selected_history={selected_history}
            setSelectedHistory={setSelectedHistory}
            setPerson={setPerson}
          />
          <div style={{ marginTop: "10px" }}>
            <SocialFiltersView
              filters={socialFilters}
              RenderSource={RenderSource}
              possible={possible}
              setPossible={setPossible}
              confirmed={confirmed}
              setConfirmed={setConfirmed}
            />
          </div>
        </Col>
        <Col span={12}>
          <Form
            {...layout}
            form={form}
            name="control-hooks"
            onFinish={onFinish}
          >
            <Form.Item
              name="keywords"
              label="Keywords"
              rules={[
                {
                  required: true,
                  message: "Please enter atleast 1 keyword!",
                },
              ]}
            >
              <Select
                mode="tags"
                style={{
                  width: "100%",
                }}
                placeholder="Type keyword and press enter"
                ref={(select) => (keywordSelect = select)}
                onChange={() => {
                  keywordSelect.blur();
                }}
              >
                {children}
              </Select>
            </Form.Item>

            <Form.Item {...tailLayout}>
              <div
                style={{
                  display: "flex",
                  alignItems: "center",
                  justifyContent: "space-between",
                }}
              >
                <Button type="primary" htmlType="submit">
                  Submit
                </Button>
                <Button htmlType="button" onClick={onReset}>
                  Clear
                </Button>
              </div>
            </Form.Item>
          </Form>
          <div>
            {/* <Skeleton active></Skeleton> */}
            {/* {loading && } */}

            {/* Confirmed matches */}
            <PersonList
              type="confirmed"
              persons={confirmed}
              loading={loading}
              person={person}
              setPerson={setPerson}
              RenderSource={RenderSource}
              ToggleConfirmedAndPossible={ToggleConfirmedAndPossible}
            />

            {/* Possible matches */}
            <PersonList
              type="possible"
              persons={possible}
              loading={loading}
              person={person}
              setPerson={setPerson}
              RenderSource={RenderSource}
              ToggleConfirmedAndPossible={ToggleConfirmedAndPossible}
            />
          </div>
        </Col>
        <Col span={5}>
          <DetailedView person={person} RenderSource={RenderSource} onFinish={onFinish} />
        </Col>
      </Row>
    </>
  );
}

export default Home;
