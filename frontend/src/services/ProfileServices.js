export async function GetAllSearches() {
  try {
    const response = await fetch(`${process.env.REACT_APP_API_URL}/searches`);
    let result = await response.json();
    return result;
  } catch (error) {
    return [];
  }
}

export async function GetSearchPersons(id) {
  try {
    const response = await fetch(
      `${process.env.REACT_APP_API_URL}/searches/${id}`
    );
    let result = await response.json();
    return result;
  } catch (error) {
    return [];
  }
}

export async function GetPersons(data) {
  const response = await fetch(`${process.env.REACT_APP_API_URL}/search`, {
    method: "POST",
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify(data),
  });
  return await response.json();
}

export async function SaveProfile(id) {
  const response = await fetch(
    `${process.env.REACT_APP_API_URL}/save_profile/${id}`
  );
  return await response.json();
}