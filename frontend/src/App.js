import { Layout, Menu, Breadcrumb } from "antd";
import React from "react";
import { Route, Routes } from "react-router-dom";

import Home from "./pages/Home";
import SavedProfiles from "./pages/SavedProfiles";

import "./App.css";
import "antd/dist/antd.min.css";
const { Header, Content, Footer } = Layout;


function App() {
  // CSS
  const css = `.site-layout-content {
    min-height: 100vh;
    padding: 24px;
    background: #fff;
    margin-top: 20px;
  }
  #components-layout-demo-top .logo {
    float: left;
    width: 120px;
    height: 31px;
    margin: 16px 24px 16px 0;
    background: rgba(255, 255, 255, 0.3);
  }
  .ant-row-rtl #components-layout-demo-top .logo {
    float: right;
    margin: 16px 0 16px 24px;
  }`;

  return (
    <Layout className="layout">
      <style>{css}</style>
      <Header
        style={{
          position: 'sticky',
          top: 0,
          zIndex: 1,
          width: '100%',
        }}
      >
        <h2 style={{ color: "white" }}>Profile Finder</h2>
        {/* <Menu
          theme="dark"
          mode="horizontal"
          defaultSelectedKeys={['2']}
          items={new Array(3).fill(null).map((_, index) => ({
            key: String(index + 1),
            label: `nav ${index + 1}`,
          }))}
        /> */}
      </Header>
      <Content
        style={{
          padding: "0 50px",
        }}
      >
        <div className="site-layout-content">
          <Routes>
            {/* <Route path="/signup" component={Signup} /> */}
            {/* <Route path="/login" component={Login} /> */}
            {/* <Route path="/dashboard" component={requireAuth(Dashboard)} /> */}
            <Route exact path="/" element={<Home />} />
            <Route exact path="/saved" element={<SavedProfiles />} />
            {/* <Route path="/resend_activation" component={ResendActivation} /> */}
            {/* <Route path="/activate/:uid/:token" component={ActivateAccount} /> */}
            {/* <Route path="/send_reset_password/" component={ResetPassword} /> */}
            {/* <Route
              path="/reset_password/:uid/:token"
              component={ResetPasswordConfirm}
            /> */}
          </Routes>
          {/* <Home /> */}
        </div>
      </Content>
      <Footer
        style={{
          textAlign: "center",
        }}
      >
        Ant Design ©2022 Ibrahimsha
      </Footer>
    </Layout>
  );
}

export default App;
