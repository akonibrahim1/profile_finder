import React, { useState, useEffect } from "react";
import {
  HistoryOutlined,
  UsergroupAddOutlined,
  DeleteTwoTone,
} from "@ant-design/icons";
import { Divider, Tag } from "antd";

import { List } from "antd";
import { GetSearchPersons } from "../services/ProfileServices";

export const SearchHistory = ({ PopulateTables, LoadHistory, histories, selected_history, setSelectedHistory, setPerson }) => {


  const LoadData = (id) => {
    setPerson({});
    setSelectedHistory(id);
    GetSearchPersons(id).then((res) => {
      PopulateTables(res);
    });
  };

  const RenderKeywords = (keys, person_count) => {
    let items = [];
    keys.split("|").map((tag) => {
      items.push(<Tag color="blue">{tag}</Tag>);
    });
    return (
      <div
        style={{
          display: "flex",
          width: "100%",
          alignItems: "center",
          justifyContent: "space-between",
        }}
      >
        <span>
          {items} - <UsergroupAddOutlined /> <b>{person_count}</b>{" "}
        </span>
        <DeleteTwoTone twoToneColor="red" />
      </div>
    );
  };

  useEffect(() => {
    LoadHistory();
  }, []);

  return (
    <>
      {histories.length > 0 && (
        <List
          id="history-list"
          size="small"
          header={
            <div
              style={{
                display: "flex",
                alignItems: "center",
              }}
            >
              <Divider orientation="left">
                <HistoryOutlined /> History
              </Divider>
            </div>
          }
          bordered
          dataSource={histories}
          renderItem={(item) => (
            <List.Item
              style={{ cursor: "pointer", justifyContent: "flex-start" }}
              onClick={() => LoadData(item.id)}
              className={selected_history == item.id ? "selected" : ""}
            >
              {RenderKeywords(item.keywords, item.person_count)}
            </List.Item>
          )}
        />
      )}
    </>
  );
};
