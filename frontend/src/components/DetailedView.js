import {
  ManOutlined,
  WomanOutlined,
  SearchOutlined,
  CheckCircleTwoTone,
  SaveOutlined
} from "@ant-design/icons";

import { Avatar, Card, Tag, Popover, Button, notification, Space } from "antd";
import { Link } from "react-router-dom";

import { SaveProfile } from "../services/ProfileServices";


export const DetailedView = ({ person, RenderSource, onFinish }) => {
  const key = `open${Date.now()}`;
  const btn = (
    <Space>
      <Link to={`/saved`}>
        <Button type="primary" size="small" onClick={() => api.destroy(key)}>
          Profiles
        </Button>
      </Link>
    </Space>
  )

  const [api, contextHolder] = notification.useNotification();



  const openNotification = (id) => {
    SaveProfile(id).then((res) => {
      api.open({
        message: person.name + ' profile saved successfully',
        description: "Click here to your saved profiles.",
        btn,
        key,
        icon: (
          <CheckCircleTwoTone
            style={{
              color: '#10e965',
            }}
          />
        ),
      });
    });
  };
  const RenderGender = (gender) => {
    if (gender == "Male") {
      return (
        <Tag icon={<ManOutlined style={{ color: "#096dd9" }} />}>Male</Tag>
      );
    } else {
      return (
        <Tag icon={<WomanOutlined style={{ color: "#eb2f96" }} />}>Female</Tag>
      );
    }
  };

  const row_style = {
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between",
    marginTop: "10px",
  };
  const { Meta } = Card;
  return (
    <>
      {contextHolder}
      {person.name && (
        <>
          <div style={{ marginLeft: "50px" }}>
            <Card
              style={{
                width: 300,
              }}
              // actions={[
              //   <SaveTwoTone key="save" />,
              //   // <EditOutlined key="edit" />,
              //   // <EllipsisOutlined key="ellipsis" />,
              // ]}
              extra={
                <>
                  <Button type="primary" icon={<SaveOutlined />} onClick={() => openNotification(person.id)}>
                    Save Profile
                  </Button>
                </>
              }
            >
              <Meta
                avatar={<Avatar src={person.image} />}
                title={person.name}
                description={person.email}
              />
              <div style={{ marginTop: "20px" }}>
                <div style={row_style}>
                  <h4 style={{ marginBottom: "0px" }}>Full Name</h4>{" "}
                  <span>{person.first_name + " " + person.last_name}</span>
                  <Popover content={"Search by name : " + person.first_name + " " + person.last_name} trigger="hover">
                    <SearchOutlined
                      style={{ color: "#096dd9" }}
                      onClick={() => onFinish({ "keywords": [person.first_name + " " + person.last_name] })} />
                  </Popover>
                </div>
                <div style={row_style}>
                  <h4 style={{ marginBottom: "0px" }}>First Name</h4>{" "}
                  <span>{person.first_name}</span>
                </div>
                <div style={row_style}>
                  <h4 style={{ marginBottom: "0px" }}>Last Name</h4>{" "}
                  <span>
                    <div>{person.last_name}</div>
                  </span>
                </div>
                <div style={row_style}>
                  <h4 style={{ marginBottom: "0px" }}>Gender</h4>{" "}
                  <span>{RenderGender(person.gender)}</span>
                </div>
                <div style={row_style}>
                  <h4 style={{ marginBottom: "0px" }}>Phone</h4>{" "}
                  <span>{person.phone}</span>
                  <Popover content={"Search by phone : " + person.phone} trigger="hover">
                    <SearchOutlined
                      style={{ color: "#096dd9" }}
                      onClick={() => onFinish({ "keywords": [person.phone] })} />
                  </Popover>
                </div>
                <div style={row_style}>
                  <h4 style={{ marginBottom: "0px" }}>Email</h4>{" "}
                  <span>{person.email}</span>
                  <Popover content={"Search by email : " + person.email} trigger="hover">
                    <SearchOutlined
                      style={{ color: "#096dd9" }}
                      onClick={() => onFinish({ "keywords": [person.email] })} />
                  </Popover>
                </div>
                <div style={row_style}>
                  <h4 style={{ marginBottom: "0px" }}>Source</h4>{" "}
                  <span>{RenderSource(person.source)}</span>
                </div>
                <div style={row_style}>
                  <h4 style={{ marginBottom: "0px" }}>Country</h4>{" "}
                  <span>{person.country}</span>
                </div>
                <div style={{ marginTop: "10px" }}>
                  <h4 style={{ marginBottom: "0px" }}>Info</h4>{" "}
                </div>
                <span>{person.info}</span>
              </div>
            </Card>
          </div>
        </>
      )}
    </>
  );
};
