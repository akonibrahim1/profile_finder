import { Badge, Divider, List, Switch } from "antd";

export const SocialFiltersView = ({ filters, RenderSource, possible, setPossible, confirmed, setConfirmed }) => {
  const LoadData = (event, source) => {
    let possibles = possible.map((p_person) => {
      var temp = Object.assign({}, p_person);
      if (temp.source == source) {
        temp.hide = !event;
      }
      return temp;
    });
    let confirms = confirmed.map((p_person) => {
      var temp = Object.assign({}, p_person);
      if (temp.source == source) {
        temp.hide = !event;
      }
      return temp;
    });
    setConfirmed(confirms);
    setPossible(possibles);
  };

  return (
    <div>
      {filters != undefined && (
        <List
          size="small"
          header={
            <div
              style={{
                display: "flex",
                alignItems: "center",
              }}
            >
              <Divider orientation="left">Social Media</Divider>
            </div>
          }
          bordered
          dataSource={Object.keys(filters)}
          renderItem={(item) => (
            <List.Item
              style={{ cursor: "pointer", justifyContent: "flex-start" }}
            >
              <span
                style={{
                  display: "flex",
                  alignItems: "center",
                  justifyContent: "space-between",
                  width: "100%",
                }}
              >
                <span>
                  {RenderSource(item)} -{" "}
                  <Badge
                    className={`site-badge-count-${filters[item]}`}
                    count={filters[item]}
                    style={{
                      backgroundColor: "rgb(230 255 234)",
                      color: "#096dd9",
                      borderColor: "#91d5ff",
                    }}
                  />{" "}
                </span>
                <Switch size="small" defaultChecked onChange={(e) => LoadData(e, item)} />
              </span>
            </List.Item>
          )}
        />
      )}
    </div>
  );
};
