import { List, Skeleton, Avatar, PageHeader } from "antd";

import {
  PlusSquareTwoTone,
  DeleteTwoTone,
  MinusSquareTwoTone,
} from "@ant-design/icons";

export const PersonList = ({
  type,
  persons,
  loading,
  person,
  setPerson,
  RenderSource,
  ToggleConfirmedAndPossible,
}) => {
  const Heading = () => {
    if (type == "confirmed") {
      return "Confirmed Matches";
    }
    return "Possible Matches";
  };

  var button = null;
  if (type == "confirmed") {
    button = (
      <MinusSquareTwoTone style={{ cursor: "pointer", fontSize: "16px" }} />
    );
  } else {
    button = (
      <PlusSquareTwoTone style={{ cursor: "pointer", fontSize: "16px" }} />
    );
  }
  return (
    <>
      {persons.length > 0 && (
        <div style={{ marginLeft: "50px", paddingTop: "20px" }}>
          <PageHeader
            className="site-page-header"
            title={Heading()}
            style={{ padding: "0px" }}
          />
          <List
            className="person-list"
            itemLayout="horizontal"
            dataSource={persons}
            renderItem={(item) => (
              <>
                {!item.hide &&
                  <List.Item
                    style={{ padding: "5px" }}
                    onClick={() => setPerson(item)}
                    className={person.id == item.id ? "selected" : ""}
                  >
                    <Skeleton loading={loading} avatar active>
                      <List.Item.Meta
                        style={{ width: "20%", flex: "unset" }}
                        avatar={<Avatar src={item.image} />}
                        title={<h4>{item.name}</h4>}
                        description={item.email}
                      />
                      <div>{item.phone}</div>
                      <div>{RenderSource(item.source)}</div>
                      <span>
                        <DeleteTwoTone
                          twoToneColor="red"
                          style={{ cursor: "pointer", fontSize: "16px" }}
                        />{" "}
                        <span
                          onClick={() => ToggleConfirmedAndPossible(item, type)}
                        >
                          {button}
                        </span>
                      </span>
                    </Skeleton>
                  </List.Item>
                }
              </>
            )}
          />
        </div>
      )}
    </>
  );
};
